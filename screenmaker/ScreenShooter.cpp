#include "ScreenShooter.h"

using namespace ScreenShooterSpace;

#ifdef _WIN32

WinScreenShooter::WinScreenShooter():hDc_(GetDC(0)),hDcDst_(CreateCompatibleDC(hDc_)),lpBMPfile(NULL)
{}

void WinScreenShooter::Shoot()
{
	int width = GetDeviceCaps(hDc_,HORZRES);
	int height = GetDeviceCaps(hDc_,VERTRES);	
	Shoot(0,0,width,height);
}

void WinScreenShooter::Shoot(int x1,int y1,int x2,int y2)
{
	HBITMAP hBitmap = CreateCompatibleBitmap(hDc_,x2-x1,y2-y1);
	SelectObject(hDcDst_,hBitmap);

	BitBlt(hDcDst_,0,0,x2-x1,y2-y1,hDc_,x1,y1,SRCCOPY);

	BITMAPINFOHEADER bih = {0};
	bih.biSize  = sizeof(BITMAPINFOHEADER);
	bih.biWidth = x2-x1;
	bih.biHeight = y2-y1;
	bih.biPlanes = 1;
	bih.biBitCount = 32;
	bih.biCompression = BI_RGB;
	bih.biSizeImage = ((bih.biWidth * bih.biBitCount + 31) / 32) * 4 * bih.biHeight;

	BITMAPFILEHEADER bfh = {0};
	bfh.bfType = 0x4D42;
	bfh.bfSize = bih.biSizeImage;
	bfh.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	lpBMPfileSize = bfh.bfOffBits + bih.biSizeImage;

	delete[] lpBMPfile;
	lpBMPfile = new char[lpBMPfileSize];
	memcpy(lpBMPfile,(char *)&bfh,sizeof(BITMAPFILEHEADER));
	memcpy(lpBMPfile+sizeof(BITMAPFILEHEADER),(char *)&bih,sizeof(BITMAPINFOHEADER));
	GetDIBits(hDcDst_,hBitmap,0,bih.biHeight,(lpBMPfile+bfh.bfOffBits),(BITMAPINFO*)&bih,DIB_RGB_COLORS);

	DeleteObject(hBitmap);
}

WinScreenShooter::~WinScreenShooter()
{
	delete[] lpBMPfile;
	DeleteDC(hDcDst_);
	ReleaseDC(0,hDc_);
}

#endif