#include "TrayIcon.h"

using namespace TrayIconSpace;

TrayIcon::TrayIcon(const HWND hWnd,const UINT uId,const UINT uCallback,const HINSTANCE hInstance,const DWORD iconId,const DWORD menuId,const char *lpHint/* = NULL*/)
	:hwnd(hWnd),uid(uId),ucallback(uCallback),
	hIcon(LoadIconA(hInstance,MAKEINTRESOURCEA(iconId))),
	hMenu(LoadMenuA(hInstance,MAKEINTRESOURCEA(menuId)))
{
	memset((char *)&nid,0,sizeof(NOTIFYICONDATA));
	nid.cbSize = sizeof(NOTIFYICONDATA);
	nid.hWnd = hWnd;
	nid.uID = uId;
	nid.uCallbackMessage = uCallback;
	nid.hIcon = hIcon;
	nid.uFlags = NIF_ICON|NIF_MESSAGE;
	if(lpHint != NULL)
	{
		strncpy(nid.szTip,lpHint,sizeof(nid.szTip));
		nid.uFlags |= NIF_TIP;
	}

	Shell_NotifyIcon(NIM_ADD,&nid);
}

TrayIcon::~TrayIcon()
{
	Shell_NotifyIcon(NIM_DELETE,&nid);
	DestroyIcon(hIcon);
	DestroyMenu(hMenu);
}

bool TrayIcon::IsIconEvent(const HWND hWnd,const UINT message,const WPARAM wParam) const
{
	return (hWnd == hwnd && message == ucallback && wParam == uid);
}

void TrayIcon::ShowContextMenu(const int submenu) const
{
	HMENU hPopup = GetSubMenu(hMenu,submenu);
	SetForegroundWindow(hwnd);
	
	POINT pt;
	GetCursorPos(&pt);
	TrackPopupMenu(hPopup,0,pt.x,pt.y,0,hwnd,NULL);
}