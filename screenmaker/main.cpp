#define _WIN32_WINNT 0x0400

#include "HttpUploader.h"
#include "ScreenShooter.h"
#include "TrayIcon.h"

#include <windows.h>
#include <iostream>
#include <string>
#include "resource.h"

char szClassName[] = "TrayLoader";

#define URL "localhost/index.php?act=1"
#define FORM_NAME "filename"

void CopyToClipboard(const char *data)
{
	OpenClipboard(0);
	EmptyClipboard();
	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE,strlen(data)+1);
	void *ptr = GlobalLock(hGlobal);
	memcpy(ptr,data,strlen(data)+1);
	GlobalUnlock(hGlobal);
	SetClipboardData(CF_TEXT,hGlobal);
	CloseClipboard();
}

void UploadFile(HWND hwnd,const char *fileName = NULL)
{
	char szFile[MAX_PATH] = {0};
	BOOL isSuccess = false;

	if(fileName == NULL)
	{
		OPENFILENAME ofn = {0};
		
		ofn.lStructSize = sizeof(ofn);
		ofn.hwndOwner = hwnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = "All\0*.*\0";
		ofn.nFilterIndex = 1;
		ofn.Flags = OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST;

		isSuccess = GetOpenFileName(&ofn);
	}
	else
	{
		strncpy(szFile,fileName,sizeof(szFile));
		isSuccess = TRUE;
	}

	if(isSuccess)
	{
		HANDLE hFile = CreateFileA(szFile,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,NULL,NULL);
		DWORD dwSize = GetFileSize(hFile,NULL);
		DWORD dwio;
		char *fileContent = new char[dwSize];
		ReadFile(hFile,fileContent,dwSize,&dwio,NULL);

		std::string fileName(szFile);
		fileName = fileName.substr(fileName.find_last_of("\\")+1,fileName.length());

		HttpUploaderSpace::WinHttpUploader whu;
		whu.UploadFile(URL,FORM_NAME,fileName.c_str(),fileContent,dwio);
		CopyToClipboard(whu.GetFileUrl());

		delete []fileContent;
		CloseHandle(hFile);

		MessageBoxA(NULL,"������ ����������� � ����� ������","��������",MB_ICONINFORMATION);
	}
}

void UploadScreenShoot()
{
	ScreenShooterSpace::WinScreenShooter wss;
	wss.Shoot();
	HttpUploaderSpace::WinHttpUploader whu;
	whu.UploadFile(URL,FORM_NAME,"screenshoot.bmp",wss.GetScreenShoot(),wss.GetScreenShootSize());

	CopyToClipboard(whu.GetFileUrl());

	MessageBoxA(NULL,"������ ����������� � ����� ������","��������",MB_ICONINFORMATION);
}


HHOOK hHook;

LRESULT CALLBACK LowLevelMouseProc(int nCode,WPARAM wParam,LPARAM lParam)
{
	static bool lbutton_push = false;
	static RECT rct;
	static HDC hDc;
	static HPEN hPen;

	if(nCode >= 0)
	{
		
		PMSLLHOOKSTRUCT nfo = (PMSLLHOOKSTRUCT)lParam;
		if(wParam == WM_LBUTTONDOWN)
		{
			rct.left = rct.right = nfo->pt.x;
			rct.top = rct.bottom = nfo->pt.y;

			hDc = GetDC(0);
			hPen = CreatePen(PS_DASH,1,0);
			SelectObject(hDc,hPen);
			SetROP2(hDc,R2_NOTXORPEN);
			Rectangle(hDc,rct.left,rct.top,rct.right,rct.bottom);

			lbutton_push = true;
			return 1;
		}
		else if(wParam == WM_LBUTTONUP)
		{
			lbutton_push = false;
			UnhookWindowsHookEx(hHook);

			Rectangle(hDc,rct.left,rct.top,rct.right,rct.bottom);
			DeleteObject(hPen);
			ReleaseDC(0,hDc);

			ScreenShooterSpace::WinScreenShooter wss;
			wss.Shoot(rct.left,rct.top,nfo->pt.x,nfo->pt.y);
			HttpUploaderSpace::WinHttpUploader whu;
			whu.UploadFile(URL,FORM_NAME,"screenshoot.bmp",wss.GetScreenShoot(),wss.GetScreenShootSize());

			CopyToClipboard(whu.GetFileUrl());

			MessageBoxA(NULL,"������ ����������� � ����� ������","��������",MB_ICONINFORMATION);
		}
		else if(wParam == WM_MOUSEMOVE && lbutton_push)
		{
			Rectangle(hDc,rct.left,rct.top,rct.right,rct.bottom);
			rct.right = nfo->pt.x;
			rct.bottom = nfo->pt.y;
			Rectangle(hDc,rct.left,rct.top,rct.right,rct.bottom);
		}
	}
	return CallNextHookEx(hHook,nCode,wParam,lParam);
}

void UploadScreenShoot2()
{
	hHook = SetWindowsHookExA(WH_MOUSE_LL,LowLevelMouseProc,GetModuleHandleA(NULL),NULL);
}

TrayIconSpace::TrayIcon *ti = NULL;

LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if(ti != NULL && ti->IsIconEvent(hwnd,message,wParam))
		if(lParam == WM_RBUTTONDOWN)
			ti->ShowContextMenu(0);

	switch (message)
	{
	case WM_CREATE:
		ti = new TrayIconSpace::TrayIcon(hwnd,0,WM_USER+1,GetModuleHandleA(NULL),IDI_ICON1,IDR_MENU1);
		break;
	case WM_COMMAND:
		switch(wParam)
		{
		case ID_MAIN_40001:
			delete ti;
			PostQuitMessage(0);
			break;
		case ID_MAIN_40002://����
			UploadFile(hwnd);
			break;
		case ID_MAIN_40003://��������
			UploadScreenShoot();
			break;
		case ID_MAIN_40004://�������� �������
			UploadScreenShoot2();
			break;
		}
		break;
	case WM_DESTROY:
		delete ti;
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, message, wParam, lParam);
	}
	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, 
				   HINSTANCE hPrevInstance, 
				   LPSTR lpCmdLine, 
				   int nShowCmd)
{
	if(strcmp(lpCmdLine,"") != 0)
	{
		UploadFile(NULL,lpCmdLine);
		return 0;
	}

	HWND hwnd;
	MSG messages;
	WNDCLASSEX wincl;

	wincl.hInstance = hInstance;
	wincl.lpszClassName = szClassName;
	wincl.lpfnWndProc = WindowProcedure;
	wincl.style = CS_DBLCLKS;
	wincl.cbSize = sizeof (WNDCLASSEX);

	wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
	wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
	wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
	wincl.lpszMenuName = NULL;
	wincl.cbClsExtra = 0;
	wincl.cbWndExtra = 0;
	wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

	if (!RegisterClassEx (&wincl))
		return 0;

	hwnd = CreateWindowEx(0,szClassName,szClassName,WS_OVERLAPPEDWINDOW,CW_USEDEFAULT,CW_USEDEFAULT,544,375,HWND_DESKTOP,NULL,hInstance,NULL);

	ShowWindow (hwnd, SW_HIDE);

	while (GetMessage (&messages, NULL, 0, 0))
	{
		TranslateMessage(&messages);
		DispatchMessage(&messages);
	}

	return (int)messages.wParam;
}