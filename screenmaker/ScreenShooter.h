#ifndef SCREENSHOOTER_H
#define SCREENSHOOTER_H

#ifdef _WIN32
	#include <Windows.h>
#endif

namespace ScreenShooterSpace
{
	class ScreenShooter
	{
	public:
		virtual void Shoot() = 0;
		virtual void Shoot(int x1,int y1,int x2,int y2) = 0;
		virtual char* GetScreenShoot() const = 0;
		virtual size_t GetScreenShootSize() const = 0;
		virtual ~ScreenShooter() = 0 {};
	};

	#ifdef _WIN32
		class WinScreenShooter:public ScreenShooter
		{
		private:
			char *lpBMPfile;
			size_t lpBMPfileSize;
			HDC hDc_;
			HDC hDcDst_;
		public:
			WinScreenShooter();
			virtual void Shoot();
			virtual void Shoot(int x1,int y1,int x2,int y2);
			virtual char* GetScreenShoot() const
			{
				return lpBMPfile;
			}
			virtual size_t GetScreenShootSize() const
			{
				return lpBMPfileSize;
			}
			virtual ~WinScreenShooter();
		};
	#endif
}

#endif