#ifndef HTTPUPLOADER_H
#define HTTPUPLOADER_H

#ifdef _WIN32
	#include <WinSock2.h>
	#include <Windows.h>
	#pragma comment(lib,"ws2_32.lib")
#endif

#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>

namespace HttpUploaderSpace
{
	class HttpUploader
	{
	protected:
		void PrepareUrl(const char *uri,std::string& host,std::string& script,unsigned int& port) const;
	public:
		virtual void UploadFile(const char *url,const char *formFileName,const char *fileName,const char *fileContent,const size_t fileSize) = 0;
		virtual ~HttpUploader() = 0 {}
		virtual const char* GetFileUrl() const = 0;
	};

	#ifdef _WIN32
	class WinHttpUploader:public HttpUploader
	{
	private:
		static int count;
		std::string lastFileUrl;
	public:
		WinHttpUploader();
		~WinHttpUploader();
		virtual void UploadFile(const char *url,const char *formFileName,const char *fileName,const char *fileContent,const size_t fileSize);
		virtual const char* GetFileUrl() const;
	};
	#endif
}

#endif