#ifndef TRAYICON_H
#define TRAYICON_H

#include <Windows.h>

namespace TrayIconSpace
{
	class TrayIcon
	{
	private:
		NOTIFYICONDATA nid;
		HWND hwnd;
		HICON hIcon;
		HMENU hMenu;
		UINT uid;
		UINT ucallback;
	public:
		TrayIcon(const HWND hWnd,const UINT uId,const UINT uCallback,const HINSTANCE hInstance,const DWORD iconId,const DWORD menuId,const char *lpHint = NULL);
		bool IsIconEvent(const HWND hWnd,const UINT message,const WPARAM wParam) const;
		void ShowContextMenu(const int submenu) const;
		~TrayIcon();
	};
}

#endif