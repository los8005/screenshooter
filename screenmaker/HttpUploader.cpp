#include "HttpUploader.h"

using namespace HttpUploaderSpace;

void HttpUploader::PrepareUrl(const char *uri,std::string& host,std::string& script,unsigned int& port) const
{
	std::string url(uri);
	std::transform(url.begin(),url.end(),url.begin(),::tolower);
	if(url.compare(0,strlen("http://"),"http://") == 0)
		url = url.substr(strlen("http://"),url.length());

	if(url.find("/") == std::string::npos)
	{
		host = url;
		script = "/";
	}
	else
	{
		host = url.substr(0,url.find("/"));
		script = url.substr(url.find("/"),url.length());
	}

	if(host.find(":") == std::string::npos)
	{
		port = 80;
	}
	else
	{
		port = atoi(host.substr(host.find(":")+1,host.length()).c_str());
		host = host.substr(0,host.find(":"));
	}
}

#ifdef _WIN32

int WinHttpUploader::count = 0;

WinHttpUploader::WinHttpUploader()
{
	if(count++ == 0)
	{
		WSADATA wsaData;
		WSAStartup(MAKEWORD(2,2),&wsaData);
	}
}

WinHttpUploader::~WinHttpUploader()
{
	if(--count == 0)
		WSACleanup();
}

void WinHttpUploader::UploadFile(const char *url,const char *formFileName,const char *fileName,const char *fileContent,const size_t fileSize)
{
	std::string host,script;
	unsigned int port;
	PrepareUrl(url,host,script,port);

	const char *boundary = "----WebKitFormBoundaryojwh1HdpLKiXb3pA";
	std::stringstream ss;
	ss<<"POST "<<script<<" HTTP/1.1"<<"\r\n"<<
		"Host: "<<host;
	if(port != 80)
		ss<<":"<<port;
	ss<<"\r\n"<<
		"Content-Length: "<<fileSize+184+strlen(formFileName)+strlen(fileName)<<"\r\n"<<
		"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"<<"\r\n"<<
		"Accept-Encoding: identity"<<"\r\n"<<
		"Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4"<<"\r\n"<<
		"Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.3"<<"\r\n"<<
		"User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1309.0 Safari/537.17"<<"\r\n"<<
		"Content-Type: multipart/form-data; boundary="<<boundary<<"\r\n"<<
		"\r\n"<<
		"--"<<boundary<<"\r\n"<<
		"Content-Disposition: form-data; name=\""<<formFileName<<"\"; filename=\""<<fileName<<"\""<<"\r\n"<<
		"Content-Type: application/octet-stream"<<"\r\n"<<
		"\r\n";

	std::stringstream endss;
	endss<<"\r\n--"<<boundary<<"--"<<"\r\n";

	hostent *he = gethostbyname(host.c_str());
	SOCKET s = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	SOCKADDR_IN ssin = {0};
	ssin.sin_family = AF_INET;
	ssin.sin_addr.s_addr = *((unsigned long *)he->h_addr_list[0]);
	ssin.sin_port = htons(port);
	connect(s,(SOCKADDR *)&ssin,sizeof(ssin));

	send(s,ss.str().c_str(),static_cast<int>(ss.str().length()),0);
	
	const size_t pack_size = 65535;

	size_t dataWait = fileSize;
	
	while(dataWait != 0)
	{
		size_t amountData = (dataWait > pack_size)?pack_size:dataWait;
		send(s,fileContent,static_cast<int>(amountData),0);

		fileContent += amountData;
		dataWait -= amountData;
	}
	
	send(s,endss.str().c_str(),static_cast<int>(endss.str().length()),0);

	char answ_buffer[pack_size] = {0};
	std::string answer = "";
	while(recv(s,answ_buffer,pack_size,0) > 0)
	{
		answer += answ_buffer;
		memset(answ_buffer,0,sizeof(answ_buffer));
	}

	answer = answer.substr(answer.find("\r\n\r\n")+strlen("\r\n\r\n"),answer.length());
	answer = answer.substr(answer.find("|")+1,answer.length());
	answer = answer.substr(0,answer.find("|"));

	// HTTPHEADER\r\n\r\n...|URL|...
	lastFileUrl = answer;

	closesocket(s);
}

const char* WinHttpUploader::GetFileUrl() const
{
	return lastFileUrl.c_str();
}

#endif